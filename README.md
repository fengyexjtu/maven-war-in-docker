## 一个简单的dockerfile示例

使用springboot的项目能够更好的在云平台上构建部署,还有一些虽然也使用了maven,却不是springboot.
只能打包成war格式,放到web服务器下运行.

- 使用了springboot的项目使用


```
FROM maven:3.5-jdk-8

ADD pom.xml /tmp/build/
RUN cd /tmp/build && mvn -q dependency:resolve

ADD src /tmp/build/src
        #构建应用
RUN cd /tmp/build && mvn -q -DskipTests=true package \
        #拷贝编译结果到指定目录
        && mv target/*.jar /app.jar \
        #清理编译痕迹
        && cd / && rm -rf /tmp/build

VOLUME /tmp
EXPOSE 8080
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]
```

使用maven构建的web项目方式,打包成war格式,一键部署


```
FROM maven:3.5-jdk-8

ADD settings.xml /root/.m2/settings.xml
ADD pom.xml /tmp/build/
RUN cd /tmp/build && mvn -q dependency:resolve

ADD src /tmp/build/src
        #构建应用
RUN cd /tmp/build && mvn -q -DskipTests=true package \
        #拷贝编译结果到指定目录
        && mv target/*.war /app.war \
        #清理编译痕迹
        && cd / && rm -rf /tmp/build

RUN curl -L -o /jetty-runner.jar "http://central.maven.org/maven2/org/eclipse/jetty/jetty-runner/9.4.6.v20170531/jetty-runner-9.4.6.v20170531.jar"
FROM maven:3.5-jdk-8

ADD settings.xml /root/.m2/settings.xml
ADD pom.xml /tmp/build/
RUN cd /tmp/build && mvn -q dependency:resolve

ADD src /tmp/build/src
        #构建应用
RUN cd /tmp/build && mvn -q -DskipTests=true package \
        #拷贝编译结果到指定目录
        && mv target/*.war /app.war \
        #清理编译痕迹
        && cd / && rm -rf /tmp/build

RUN curl -L -o /jetty-runner.jar "http://central.maven.org/maven2/org/eclipse/jetty/jetty-runner/9.4.6.v20170531/jetty-runner-9.4.6.v20170531.jar"

VOLUME /tmp
EXPOSE 8080
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/jetty-runner.jar","/app.war"]

```

说明:
> 使用jetty-runner 启动war格式的web项目很方便
`jetty-runner.jar app.war`

> setting.xml 私有nexus,加快构建速度

> 示例项目springmvc,使用h2,增删改查而已.




